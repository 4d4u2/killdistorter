﻿using KillDistortion.Model;
using LMDotNet;
using System;
using KillDistortion.Utility;
using System.Linq;
using Emgu.CV.Structure;

namespace KillDistortion.Refiner
{
    /// <summary>
    /// Defines the refiner for the first class
    /// </summary>
    public class Refiner1
    {
        #region Private Variables

        private int _pointCount;
        private MCvPoint2D64f _center;
        private Parameters _parameters;
        private Distortion _distortion;
        private ErrorFinder _errorFinder;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Defines the frame points within the system
        /// </summary>
        /// <param name="parameters">The associated list of parameters</param>
        /// <param name="framePoints">The frame points that we are finding for</param>
        public Refiner1(Parameters parameters, FramePoints framePoints)
        {
            _parameters = parameters; _pointCount = framePoints.PointCount;
            _errorFinder = new ErrorFinder(parameters, framePoints);
        }

        #endregion

        #region Distortion Estimation Entry Point

        /// <summary>
        /// Defines the logic to refine the given distortion
        /// </summary>
        /// <param name="distortion">The distortion that we are refining</param>
        public void Refine(Distortion distortion)
        {
            _distortion = distortion; _center = new MCvPoint2D64f(distortion.CX, distortion.CY);
            var solver = new LMSolver(epsilon:1e-3);
            var result = solver.Minimize(CostFunction, new double[] { _distortion.K1, 0, 0 }, _pointCount);
            distortion.K1 = result.OptimizedParameters[0];
            _distortion.CX = _center.X + result.OptimizedParameters[1];
            _distortion.CY = _center.Y + result.OptimizedParameters[2];
        }

        #endregion

        #region Cost Function

        /// <summary>
        /// Defines the cost function for finding the current error associated with the given distortion
        /// </summary>
        /// <param name="parameters">The current error parameters</param>
        /// <param name="errors">The errors associated with the given parameter</param>
        private void CostFunction(double[] parameters, double[] errors)
        {
            _distortion.K1 = parameters[0]; _distortion.CX = _center.X + parameters[1]; _distortion.CY = _center.Y + parameters[2];

            var errorList = _errorFinder.Find(_distortion, _parameters, true);
            for (int i = 0; i < errorList.Count; i++) errors[i] = errorList[i];

           // var score = errorList.Sum()/ errorList.Count; Console.WriteLine("Score: " + score);
        }

        #endregion
    }
}
