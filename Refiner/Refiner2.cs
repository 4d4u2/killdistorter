﻿using KillDistortion.Model;
using KillDistortion.Utility;
using LMDotNet;
using System;
using System.Linq;

namespace KillDistortion.Refiner
{
    /// <summary>
    /// Defines the functionality associated with a full refiner
    /// </summary>
    public class Refiner2
    {
        #region Private Variables

        private int _pointCount;
        private Parameters _parameters;
        private ErrorFinder _errorFinder;
        private Distortion _distortion;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Contructor
        /// </summary>
        /// <param name="parameters">The parameters that we are working with</param>
        /// <param name="framePoints">The associted frame points</param>
        public Refiner2(Parameters parameters, FramePoints framePoints)
        {
            _parameters = parameters; _pointCount = framePoints.PointCount; _errorFinder = new ErrorFinder(parameters, framePoints);
        }

        #endregion

        #region Distortion Estimation Entry Point

        /// <summary>
        /// Defines the logic to refine the given distortion
        /// </summary>
        /// <param name="distortion">The distortion that we are refining</param>
        public void Refine(Distortion distortion)
        {
            _distortion = distortion;
            var solver = new LMSolver(epsilon: 1e-5);
            var result = solver.Minimize(CostFunction, new double[] { _distortion.K1, _distortion.K2, _distortion.P1, _distortion.P2}, _pointCount);

            var parameters = result.OptimizedParameters;
            distortion.K1 = parameters[0];
            distortion.K2 = parameters[1];
            distortion.P1 = parameters[2];
            distortion.P2 = parameters[3];
        }

        #endregion

        #region Cost Function

        /// <summary>
        /// Defines the cost function for finding the current error associated with the given distortion
        /// </summary>
        /// <param name="parameters">The current error parameters</param>
        /// <param name="errors">The errors associated with the given parameter</param>
        private void CostFunction(double[] parameters, double[] errors)
        {
            _distortion.K1 = parameters[0];
            _distortion.K2 = parameters[1];
            _distortion.P1 = parameters[2];
            _distortion.P2 = parameters[3];

            var errorList = _errorFinder.Find(_distortion, _parameters, true);
            for (int i = 0; i < errorList.Count; i++) errors[i] = errorList[i];

           // Console.WriteLine("Error: {0}:", errorList.Sum() / errorList.Count);
        }

        #endregion
    }
}
