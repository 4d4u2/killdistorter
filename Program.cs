﻿using KillDistortion.Controller;
using KillDistortion.Model;
using System;

namespace KillDistortion
{
    /// <summary>
    /// Main application entry point method
    /// </summary>
    public class Program
    {
        #region Application Entry Point Method

        /// <summary>
        /// Entry point method associated with the application
        /// </summary>
        public static void Main()
        {
            try
            {
                var arguments = new Arguments();
                new MainController(arguments).Run(); Pause();
            }
            catch (Exception exception)
            {
                Console.Error.WriteLine("ERROR: " + exception.Message); Pause();
            }
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Defines the logic to pause the screen
        /// </summary>
        private static void Pause()
        {
            Console.WriteLine(); Console.WriteLine("Press ENTER to continue...");
            Console.ReadLine();
        }

        #endregion
    }
}
