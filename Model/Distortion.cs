﻿using System;
namespace KillDistortion.Model
{
    /// <summary>
    /// Defines a container class for holding the distortion within the system
    /// </summary>
    public class Distortion
    {
        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="parameters">The parametes associated with the application</param>
        public Distortion(Parameters parameters)
        {
            K1 = K2 = P1 = P2 = 0;
            CX = parameters.Width / 2.0; CY = parameters.Height / 2.0;
            Focal = Math.Max(parameters.Width, parameters.Height);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The first radial coefficient
        /// </summary>
        public double K1 { get; set; }

        /// <summary>
        /// The second radial coefficient
        /// </summary>
        public double K2 { get; set; }

        /// <summary>
        /// The first tangential coefficient
        /// </summary>
        public double P1 { get; set; }

        /// <summary>
        /// The second tangentail coefficient
        /// </summary>
        public double P2 { get; set; }

        /// <summary>
        /// The optical center in the x-direction
        /// </summary>
        public double CX { get; set; }

        /// <summary>
        /// The optical center in the y-direction
        /// </summary>
        public double CY { get; set; }

        /// <summary>
        /// The associated focal length
        /// </summary>
        public double Focal { get; set; }

        #endregion
    }
}
