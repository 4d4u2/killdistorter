﻿namespace KillDistortion.Model
{
    /// <summary>
    /// Defines the parameters associated with the application
    /// </summary>
    public class Parameters
    {
        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="ActiveSet">The image set that we are processing</param>
        /// <param name="gridX">The grid size in the x-direction (amount of blocks)</param>
        /// <param name="gridY">The gird size in the y-direction (amount of blocks)</param>
        /// <param name="blockSize">The size of a block in millimeters</param>
        /// <param name="width">The width of the calibration image</param>
        /// <param name="height">The height of the calibration image</param>
        public Parameters(int activeSet, int gridX, int gridY, double blockSize, int width, int height)
        {
            ActiveSet = activeSet;
            GridX = gridX; GridY = gridY; BlockSize = blockSize; Width = width; Height = height;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The active set that we are processing
        /// </summary>
        public int ActiveSet { get; private set; }

        /// <summary>
        /// The grid size in the x-direction (the amount of blocks)
        /// </summary>
        public int GridX { get; private set; }

        /// <summary>
        /// The grid size in the y-direction (the amount of blocks)
        /// </summary>
        public int GridY { get; private set; }

        /// <summary>
        /// The size of a block (in mm)
        /// </summary>
        public double BlockSize { get; private set; }

        /// <summary>
        /// The width of the calibration image
        /// </summary>
        public int Width { get; private set; }

        /// <summary>
        /// The height of the calibration image
        /// </summary>
        public int Height { get; private set; }

        #endregion
    }
}