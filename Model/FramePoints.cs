﻿using KillDistortion.Model;
using System.Collections.Generic;

namespace KillDistortion.Model
{
    /// <summary>
    /// Defines the points for a given frame
    /// </summary>
    public class FramePoints
    {
        #region Constructors

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="leftPoints">The left points that we are working with</param>
        /// <param name="rightPoints">The right points that we are working with</param>
        public FramePoints(List<GridPoint> leftPoints, List<GridPoint> rightPoints) 
        {
            LeftPoints = leftPoints; RightPoints = rightPoints;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Retrieve the collection of left points
        /// </summary>
        public List<GridPoint> LeftPoints { get; private set; }

        /// <summary>
        /// Retrieve the collection of right points
        /// </summary>
        public List<GridPoint> RightPoints { get; private set; }

        /// <summary>
        /// Retrieve the total number of points
        /// </summary>
        public int PointCount { get { return LeftPoints.Count + RightPoints.Count; } }

        #endregion
    }
}
