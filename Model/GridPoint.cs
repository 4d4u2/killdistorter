﻿using Emgu.CV.Structure;

namespace KillDistortion.Model
{
    /// <summary>
    /// A grid point within the system
    /// </summary>
    public class GridPoint
    {
        #region Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="scenePoint">The scene point that we are working with</param>
        /// <param name="imagePoint">The image point that we are working with</param>
        public GridPoint(MCvPoint3D64f scenePoint, MCvPoint2D64f imagePoint)
        {
            ScenePoint = scenePoint; ImagePoint = imagePoint; OriginalImagePoint = imagePoint;
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// The scene point of the grid point
        /// </summary>
        public MCvPoint3D64f ScenePoint { get; set; }

        /// <summary>
        /// The image point of the grid that we are working with
        /// </summary>
        public MCvPoint2D64f ImagePoint { get; set; }

        /// <summary>
        /// The original image point of the grid that we are working with
        /// </summary>
        public MCvPoint2D64f OriginalImagePoint { get; set; }

        #endregion
    }
}