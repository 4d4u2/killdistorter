﻿using Emgu.CV;
using KillDistortion.Model;
using KillDistortion.Refiner;
using KillDistortion.Utility;
using System;
using System.Linq;

namespace KillDistortion.Controller
{
    /// <summary>
    /// The main controller associated with the application
    /// </summary>
    public class MainController
    {
        #region Private Variables

        private Arguments _arguments;
        private FramePoints _points1;
        private FramePoints _points2;

        #endregion

        #region Main Controller

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="arguments">The arguments that have been passed to the application</param>
        public MainController(Arguments arguments)
        {
            _arguments = arguments;
            _points1 = PointLoader.Load(arguments.PointSetPath1, arguments.Parameters);
            _points2 = PointLoader.Load(arguments.PointSetPath2, arguments.Parameters);
        }

        #endregion

        #region Execution Logic
        
        /// <summary>
        /// Main execution method associated with the application
        /// </summary>
        public void Run()
        {
            Console.WriteLine("Processing the Left Frame...");
            ProcessFrame(_points1, _arguments.DistortionPath1, _arguments.OutputPointPath1);

            Console.WriteLine("Processing the right frame...");
            ProcessFrame(_points2, _arguments.Distortionpath2, _arguments.OutputPointPath2);
        }

        /// <summary>
        /// Defines the logic to process the associated frame
        /// </summary>
        /// <param name="points">The points that we are processing</param>
        /// <param name="distortionSavePath">The location to save the result</param>
        /// <param name="pointSavePath">The path we are saving the points to</param>
        private void ProcessFrame(FramePoints points, string distortionSavePath, string pointSavePath)
        {
            // Helper Functions
            var distortion = new Distortion(_arguments.Parameters);
            var errorFinder = new ErrorFinder(_arguments.Parameters, points);

            // Perform the first refinement
            new Refiner1(_arguments.Parameters, points).Refine(distortion);

            // Show the result of the first refinement
            var score = errorFinder.Find(distortion, _arguments.Parameters);
            Console.WriteLine("Score after refiner 1: " + (score.Sum() / score.Count));

            // Perform the second refinement
            new Refiner2(_arguments.Parameters, points).Refine(distortion);

            // Show the result of the second refinement
            score = errorFinder.Find(distortion, _arguments.Parameters);
            Console.WriteLine("Score after refiner 2: " + (score.Sum() / score.Count));

            // Save calibration results to disk
            Console.WriteLine("Writing the results to disk...");
            DistortionSaver.Save(distortionSavePath, distortion);

            // Save the undistorted points to disk
            Console.WriteLine("Writing points to disk....");
            PointSaver.Save(pointSavePath, distortion, points);
        }

        #endregion
    }
}