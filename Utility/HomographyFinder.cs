﻿using Emgu.CV;
using KillDistortion.Model;
using System.Collections.Generic;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Define a module that is capable of mapping 3D points to a 2D points
    /// </summary>
    public class HomographyFinder
    {
        #region Finder Entry Point

        /// <summary>
        /// Find the conversion mapper for the grid corners
        /// </summary>
        /// <param name="corners">The corners of the grid that we are finding the homography for</param>
        /// <returns>The resultant Homography matrix</returns>
        public static Matrix<double> Find(List<GridPoint> corners)
        {
            var matrix1 = BuildMatrix1(corners);
            var matrix2 = BuildVector1(corners);
            return FindSolution(matrix1, matrix2);
        }

        #endregion

        #region Matrix builders

        /// <summary>
        /// Build a vector for finding the homography
        /// </summary>
        /// <param name="corners">The corners that we are building the vector from</param>
        /// <returns>The vector that makes the corners</returns>
        private static Matrix<double> BuildVector1(List<GridPoint> corners)
        {
            var result = new Matrix<double>(8, 1);

            for (int i = 0; i < 4; i++)
            {
                result.Data[i * 2 + 0, 0] = corners[i].ImagePoint.X;
                result.Data[i * 2 + 1, 0] = corners[i].ImagePoint.Y;
            }

            return result;
        }

        /// <summary>
        /// Build a matrix for finding the homography
        /// </summary>
        /// <param name="corners">The list of fixed corners</param>
        /// <returns>The matrix used to find the homography</returns>
        private static Matrix<double> BuildMatrix1(List<GridPoint> corners)
        {
            var result = new Matrix<double>(8, 8);

            for (int i = 0; i < 4; i++)
            {
                var scenePoint = corners[i].ScenePoint;
                var imagePoint = corners[i].ImagePoint;

                UpdateMatrix(result, i * 2 + 0, scenePoint.X, scenePoint.Y, 1, 0, 0, 0, -imagePoint.X * scenePoint.X, -imagePoint.X * scenePoint.Y);
                UpdateMatrix(result, i * 2 + 1, 0, 0, 0, scenePoint.X, scenePoint.Y, 1, -imagePoint.Y * scenePoint.X, -imagePoint.Y * scenePoint.Y);
            }

            return result;
        }

        /// <summary>
        /// Update the matrix that we are dealing with
        /// </summary>
        /// <param name="matrix">The matrix that we are filling</param>
        /// <param name="row">The row that is being filled</param>
        /// <param name="col1">The value for column 1</param>
        /// <param name="col2">The value for column 2</param>
        /// <param name="col3">The value for column 3</param>
        /// <param name="col4">The value for column 4</param>
        /// <param name="col5">The value for column 5</param>
        /// <param name="col6">The value for column 6</param>
        /// <param name="col7">The value for column 7</param>
        /// <param name="col8">The value for column 8</param>
        private static void UpdateMatrix(Matrix<double> matrix, int row, double col1, double col2, double col3, double col4, double col5, double col6, double col7, double col8)
        {
            matrix.Data[row, 0] = col1;
            matrix.Data[row, 1] = col2;
            matrix.Data[row, 2] = col3;
            matrix.Data[row, 3] = col4;
            matrix.Data[row, 4] = col5;
            matrix.Data[row, 5] = col6;
            matrix.Data[row, 6] = col7;
            matrix.Data[row, 7] = col8;
        }

        #endregion

        #region Solver Logic

        /// <summary>
        /// Find a solution given the two matrices
        /// </summary>
        /// <param name="matrix1">The first solution we are finding for</param>
        /// <param name="matrix2">The second solution we are finding for</param>
        /// <returns></returns>
        private static Matrix<double> FindSolution(Matrix<double> matrix1, Matrix<double> matrix2)
        {
            var solution = new Matrix<double>(8, 1);
            CvInvoke.Solve(matrix1, matrix2, solution, Emgu.CV.CvEnum.DecompMethod.Normal);
            return RemapSolution(solution);
        }

        /// <summary>
        /// Remap the associated solution
        /// </summary>
        /// <param name="solution">The solution that we are remapping</param>
        /// <returns>The solution that we are remapping</returns>
        private static Matrix<double> RemapSolution(Matrix<double> solution)
        {
            var result = new Matrix<double>(3, 3);

            result.Data[0, 0] = solution.Data[0, 0];
            result.Data[0, 1] = solution.Data[1, 0];
            result.Data[0, 2] = solution.Data[2, 0];
            result.Data[1, 0] = solution.Data[3, 0];
            result.Data[1, 1] = solution.Data[4, 0];
            result.Data[1, 2] = solution.Data[5, 0];
            result.Data[2, 0] = solution.Data[6, 0];
            result.Data[2, 1] = solution.Data[7, 0];
            result.Data[2, 2] = 1;

            return result;
        }

        #endregion
    }
}
