﻿using Emgu.CV;
using Emgu.CV.Structure;
using KillDistortion.Model;
using System.Collections.Generic;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Generate a set of points from a given homgraphy
    /// </summary>
    public class PointGenerator
    {
        #region Point Generation Logic

        /// <summary>
        /// Generate a list of points from the given homography
        /// </summary>
        /// <param name="H">The homography that we are generating the points from</param>
        /// <param name="parameters">The paramteers of the grids that we are generating estimate points for</param>
        /// <returns>The list of points that we have generated</returns>
        public static List<GridPoint> Generate(Matrix<double> H, Parameters parameters)
        {
            var result = new List<GridPoint>();

            for (int row = 0; row <= parameters.GridY; row++)
            {
                for (int column = 0; column <= parameters.GridX; column++)
                {
                    var X = column * parameters.BlockSize;
                    var Y = (parameters.GridY - row) * parameters.BlockSize;
                    var Z = 0;
                    var scenePoint = new MCvPoint3D64f(X, Y, Z);

                    var u = H.Data[0, 0] * X + H.Data[0, 1] * Y + H.Data[0, 2];
                    var v = H.Data[1, 0] * X + H.Data[1, 1] * Y + H.Data[1, 2];
                    var a = H.Data[2, 0] * X + H.Data[2, 1] * Y + H.Data[2, 2];
                    u = u / a; v = v / a;
                    var imagePoint = new MCvPoint2D64f(u, v);
                    result.Add(new GridPoint(scenePoint, imagePoint));
                }
            }

            return result;
        }
        
        #endregion
    }
}