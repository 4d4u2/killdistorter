﻿using KillDistortion.Model;
using System.Collections.Generic;
using System.IO;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Defines the logic to save the set of points to disk
    /// </summary>
    public class PointSaver
    {
        #region Save Logic

        /// <summary>
        /// Save the grid parameters to disk
        /// </summary>
        /// <param name="path">The path that we are saving to</param>
        /// <param name="distortion">The derived distortion</param>
        /// <param name="points">The ponts that we are saving</param>
        public static void Save(string path, Distortion distortion, FramePoints points)
        {
            var distorter = new Distorter();
            var pointList = new List<GridPoint>(points.LeftPoints); pointList.AddRange(points.RightPoints);
            var uPoints = pointList.ConvertAll(point => new GridPoint(point.ScenePoint, distorter.UnDistort(distortion, point.ImagePoint)));
            WritePoints(path, uPoints);
        }

        #endregion

        #region Grid Saving Functionality

        /// <summary>
        /// Write the associated set of points to disk
        /// </summary>
        /// <param name="path">The name of the file that we are writing</param>
        /// <param name="points">The points that we have found</param>
        private static void WritePoints(string path, List<GridPoint> points)
        {
            using (var writer = new StreamWriter(path))
            {
                foreach (var point in points)
                {
                    writer.WriteLine("{0} {1}", point.ImagePoint.X, point.ImagePoint.Y);
                }
            }
        }

        #endregion
    }
}
