﻿using Emgu.CV;
using Emgu.CV.Structure;
using KillDistortion.Model;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Display the points we are working on the screen
    /// </summary>
    public class Visualizer
    {
        #region Private Variables

        private FramePoints _frame;
        private List<GridPoint> _estimated;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Main Constructor
        /// </summary>
        /// <param name="frame">The frame that we are visualizing</param>
        public Visualizer(FramePoints frame) 
        {
            _frame = frame; _estimated = new List<GridPoint>();
        }

        #endregion

        #region Render

        /// <summary>
        /// Render the calibration points (and later estimated points)
        /// </summary>
        /// <param name="parameters">The parameters of the screen</param>
        public void Render(Parameters parameters) 
        {
           var circleSize = 15; 

            var image = new Image<Bgr, byte>(parameters.Width, parameters.Height); image.SetZero();
            var points = _frame.LeftPoints.ConvertAll(point => GetPoint(point));
            points.AddRange(_frame.RightPoints.ConvertAll(point =>GetPoint(point)));

            foreach (var point in points)
            {
               CvInvoke.Circle(image, point, 3, new MCvScalar(0, 255, 0), 3);
            }

            var estPoints = _estimated.ConvertAll(point => GetPoint(point));
            estPoints.ForEach(point => CvInvoke.Circle(image, point, circleSize, new MCvScalar(0, 0, 255), circleSize));

            var small = new Mat(); CvInvoke.Resize(image, small, new Size(1000,1000));

            CvInvoke.Imshow("Points", small.ToImage<Bgr, byte>());
        }

        #endregion

        #region Estimated Points Helpers

        /// <summary>
        /// Clear the estimated points
        /// </summary>
        public void Clear() 
        {
            _estimated.Clear();
        }

        /// <summary>
        /// Add estimated points
        /// </summary>
        /// <param name="points">The points that we are adding</param>
        public void AddPoints(List<GridPoint> points) 
        {
            _estimated.AddRange(points);
        }

        #endregion

        #region Helper Utility Functions

        /// <summary>
        /// Retrieve a point from a given grid point
        /// </summary>
        /// <param name="point">The point we are extracting frame</param>
        /// <returns>The resultant point</returns>
        private Point GetPoint(GridPoint point)
        {
            var u = (int)Math.Round(point.ImagePoint.X);
            var v = (int)Math.Round(point.ImagePoint.Y);
            return new Point(u, v);
        }

        #endregion
    }
}
