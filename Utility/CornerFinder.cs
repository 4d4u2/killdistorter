﻿using KillDistortion.Model;
using System.Collections.Generic;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Defines the logic to find corners for the various point sets
    /// </summary>
    public class CornerFinder
    {
        #region Corner Extractor Algorithm

        /// <summary>
        /// Extract the corners from the grid
        /// </summary>
        /// <param name="grid">The source grid</param>
        /// <param name="parameters">Parameters of the grid</param>
        /// <returns>The list of corners</returns>
        public static List<GridPoint> Find(List<GridPoint> grid, Parameters parameters) 
        {
            var corners = new List<GridPoint>
            {
                grid[0],
                grid[parameters.GridX],
                grid[grid.Count - 1],
                grid[grid.Count - parameters.GridX - 1]
            };

            return corners;
        }

        #endregion
    }
}