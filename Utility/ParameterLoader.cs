﻿using Emgu.CV;
using KillDistortion.Model;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Defines the parameters required to load the given class
    /// </summary>
    public class ParameterLoader
    {
        #region Load the parameters from disk

        /// <summary>
        /// Load the parameters from disk
        /// </summary>
        /// <param name="path">The path where we are loading the parameters from</param>
        /// <returns>The parameters that we have found on disk</returns>
        public static Parameters Load(string path)
        {
            using (var reader = new FileStorage(path, FileStorage.Mode.Read | FileStorage.Mode.FormatXml))
            {
                var activeSet = reader.GetNode("ActiveSet").ReadInt();
                var gridX = reader.GetNode("GridX").ReadInt();
                var gridY = reader.GetNode("GridY").ReadInt();
                var blockSize = reader.GetNode("BlockSize").ReadDouble();
                var width = reader.GetNode("Width").ReadInt();
                var height = reader.GetNode("Height").ReadInt();
                return new Parameters(activeSet, gridX, gridY, blockSize, width, height);
            }
        }

        #endregion
    }
}
