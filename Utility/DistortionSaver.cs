﻿using Emgu.CV;
using KillDistortion.Model;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Defines the logic to save the distortion to disk
    /// </summary>
    public class DistortionSaver
    {
        #region Save Logic

        /// <summary>
        /// Save the associated distortion to disk
        /// </summary>
        /// <param name="path">The path that we are saving the distortion to</param>
        /// <param name="distortion">The distoriton values that we are saving</param>
        public static void Save(string path, Distortion distortion) 
        {
            using (var writer = new FileStorage(path, FileStorage.Mode.Write | FileStorage.Mode.FormatXml)) 
            {
                writer.Write(distortion.K1, "K1");
                writer.Write(distortion.K2, "K2");
                writer.Write(distortion.P1, "P1");
                writer.Write(distortion.P2, "P2");
                writer.Write(distortion.CX, "CX");
                writer.Write(distortion.CY, "CY");
                writer.Write(distortion.Focal, "Focal");
            }
        }
 
        #endregion
    }
}