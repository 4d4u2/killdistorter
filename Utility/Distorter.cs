﻿using Emgu.CV.Structure;
using KillDistortion.Model;
using LMDotNet;
using System;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Defines the logic to perform Zhang Distortion
    /// </summary>
    public class Distorter
    {
        #region Private Variables

        private Distortion _distortion;
        private MCvPoint2D64f _expectedCoordinate;

        #endregion

        #region Add Distortion

        /// <summary>
        /// Defines the logic to distort a given point
        /// </summary>
        /// <param name="distortion">The distoriton parameters that we are working with</param>
        /// <param name="point">The point that we are distorting</param>
        /// <returns>The distorted location of the point</returns>
        public MCvPoint2D64f Distort(Distortion distortion, MCvPoint2D64f point)
        {
            var screen = Image2Screen(distortion, point);
            var r2 = screen.X * screen.X + screen.Y * screen.Y; var r4 = r2 * r2;

            var k1 = distortion.K1;  var k2 = distortion.K2; var p1 = distortion.P1; var p2 = distortion.P2;
            var xu = screen.X; var yu = screen.Y;

            var xd = xu * (1 + k1 * r2 + k2 * r4) + p2 * (r2 + 2 * xu * xu) + 2 * p1 * xu * yu;
            var yd = yu * (1 + k1 * r2 + k2 * r4) + p1 * (r2 + 2 * yu * yu) + 2 * p2 * xu * yu;

            return Screen2Image(distortion, new MCvPoint2D64f(xd,yd));
        }

        #endregion

        #region Remove Distortion

        /// <summary>
        /// Defines the logic to undistort a given point
        /// </summary>
        /// <param name="point">The point that we are undistorting</param>
        /// <returns>The undistorted location of the given point</returns>
        public MCvPoint2D64f UnDistort(Distortion distortion, MCvPoint2D64f point)
        {
            _expectedCoordinate = point; _distortion = distortion;
            var solver = new LMSolver(); 
            var result = solver.Solve(CostFunction, new double[] { _expectedCoordinate.X, _expectedCoordinate.Y });
            return new MCvPoint2D64f(result.OptimizedParameters[0], result.OptimizedParameters[1]);
        }

        /// <summary>
        /// The function that we are minimizing
        /// </summary>
        /// <param name="parameters">The parameters of the function</param>
        /// <param name="residuals">The associated residuals</param>
        private void CostFunction(double[] parameters, double[] residuals)
        {
            var point = new MCvPoint2D64f(parameters[0], parameters[1]);
            var solution = Distort(_distortion, point);
            residuals[0] = Math.Sqrt ( (_expectedCoordinate.X - solution.X) * (_expectedCoordinate.X - solution.X));
            residuals[1] = Math.Sqrt ( (_expectedCoordinate.Y - solution.Y) * (_expectedCoordinate.Y - solution.Y) );
        }

        #endregion

        #region Conversion Utilities

        /// <summary>
        /// Convert a screen coordinate to an image coordinate
        /// </summary>
        /// <param name="distortion">The associated set of distortion parameters</param>
        /// <param name="point">The point that we are converting</param>
        /// <returns>The resultant coordinate</returns>
        private static MCvPoint2D64f Screen2Image(Distortion distortion, MCvPoint2D64f point)
        {
            var x = point.X * distortion.Focal + distortion.CX;
            var y = point.Y * distortion.Focal + distortion.CY;
            return new MCvPoint2D64f(x, y);
        }

        /// <summary>
        /// Convert an image coordinate to a screen coordinate
        /// </summary>
        /// <param name="distortion">The distortion parameters that we are dealing with</param>
        /// <param name="point">The point that we are converting</param>
        /// <returns>The resultant coordinate</returns>
        private static MCvPoint2D64f Image2Screen(Distortion distortion, MCvPoint2D64f point)
        {
            var x = (point.X - distortion.CX) / distortion.Focal;
            var y = (point.Y - distortion.CY) / distortion.Focal;
            return new MCvPoint2D64f(x, y);
        }

        #endregion
    }
}