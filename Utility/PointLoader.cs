﻿using Emgu.CV.Structure;
using KillDistortion.Model;
using System.Collections.Generic;
using System.IO;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Defines the logic to load a set of points from disk
    /// </summary>
    public class PointLoader
    {
        #region Loader Logic

        /// <summary>
        /// Defines the logic to load points from disk
        /// </summary>
        /// <param name="path">The path that we are loading from</param>
        /// <param name="parameters">The parameters associated with the loader</param>
        /// <returns>The resultant list of grid points</returns>
        public static FramePoints Load(string path, Parameters parameters)
        {
            var leftPoints = new List<GridPoint>();
            var rightPoints = new List<GridPoint>();

            using (var reader = new StreamReader(path))
            {
                FillPoints(leftPoints, reader, parameters);
                FillPoints(rightPoints, reader, parameters);
            }

            return new FramePoints(leftPoints, rightPoints);
        }

        #endregion

        #region Utility Methods

        /// <summary>
        /// Defines the logic to fill the set of points
        /// </summary>
        /// <param name="points">The points that we are filling from</param>
        /// <param name="reader">The reader that we are using to fill the points</param>
        /// <param name="parameters">The list of parameters that we are filling from</param>
        private static void FillPoints(List<GridPoint> points, StreamReader reader, Parameters parameters)
        {
            for (int row = 0; row <= parameters.GridY; row++)
            {
                for (int column = 0; column <= parameters.GridX; column++)
                {
                    var X = column * parameters.BlockSize;
                    var Y = (parameters.GridY - row) * parameters.BlockSize;
                    var Z = 0;
                    var scenePoint = new MCvPoint3D64f(X, Y, Z);

                    var line = reader.ReadLine();
                    var parts = line.Split(" ".ToCharArray());

                    var u = double.Parse(parts[0]);
                    var v = double.Parse(parts[1]);
                    var imagePoint = new MCvPoint2D64f(u, v);

                    points.Add(new GridPoint(scenePoint, imagePoint));
                }
            }
        }

        #endregion
    }
}