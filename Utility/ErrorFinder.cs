﻿using Emgu.CV;
using KillDistortion.Model;
using System;
using System.Collections.Generic;

namespace KillDistortion.Utility
{
    /// <summary>
    /// Derive the error from two point sets
    /// </summary>
    public class ErrorFinder
    {
        #region Private Variables

        private Parameters _parameters;
        private FramePoints _points;
        private Visualizer _visualizer;

        #endregion

        #region Main Constructor

        /// <summary>
        /// Defines the error finder
        /// </summary>
        /// <param name="points">The points that we are working with</param>
        public ErrorFinder(Parameters parameters, FramePoints points) 
        {
            _visualizer = new Visualizer(points); _points = points; _parameters = parameters;
        }

        #endregion


        #region Find Errors

        /// <summary>
        /// Entry point for finding the errors associated with the two point sets
        /// </summary>
        /// <param name="distortion">The distortion that we are applying</param>
        /// <param name="parameters">The parameters associated with the variables</param>
        /// <param name="framePoints">The frame points associated with the application</param>
        /// <returns>The resultant list of errors</returns>
        public List<double> Find(Distortion distortion, Parameters parameters, bool display = false)
        {
            _visualizer.Clear();
            var list1 = FindErrors(distortion, parameters, _points.LeftPoints);
            var list2 = FindErrors(distortion, parameters, _points.RightPoints);
            list1.AddRange(list2);

            if (display) 
            {
                _visualizer.Render(_parameters); CvInvoke.WaitKey(30);
            }

            return list1;
        }

        #endregion

        #region Find the errors associated with the given function

        /// <summary>
        /// Find the errors associated with the given fucntion
        /// </summary>
        /// <param name="distortion">The distortion that we are scoring</param>
        /// <param name="parameters">The parameters that we are finding the variables for</param>
        /// <param name="points">The points that we are calculating with</param>
        /// <returns>The list of errors associated with the score</returns>
        private List<double> FindErrors(Distortion distortion, Parameters parameters, List<GridPoint> points)
        {
            var distorter = new Distorter();
            var corners = CornerFinder.Find(points, parameters);
            var ucorners = corners.ConvertAll(corner => new GridPoint(corner.ScenePoint, distorter.UnDistort(distortion, corner.ImagePoint)));
            var H = HomographyFinder.Find(ucorners);
            var genPoints = PointGenerator.Generate(H, parameters);
            var estPoints = genPoints.ConvertAll(point => new GridPoint(point.ScenePoint, distorter.Distort(distortion, point.ImagePoint)));
            _visualizer.AddPoints(estPoints);
            return BuildErrorList(points, estPoints);
        }

        #endregion

        #region Distance Finder Utility Method

        /// <summary>
        /// Find the distance between the two given point sets
        /// </summary>
        /// <param name="set1">The first set of points</param>
        /// <param name="set2">The second set of points</param>
        /// <returns>The resultant score from the two sets</returns>
        private List<double> BuildErrorList(List<GridPoint> set1, List<GridPoint> set2)
        {
            var result = new List<double>();
            
            for (int i = 0; i < set1.Count; i++)
            {
                var xDiff = set1[i].ImagePoint.X - set2[i].ImagePoint.X;
                var yDiff = set1[i].ImagePoint.Y - set2[i].ImagePoint.Y;
                var distance = Math.Sqrt(xDiff * xDiff + yDiff * yDiff);
                result.Add(distance);
            }

            return result;
        }

        #endregion
    }
}